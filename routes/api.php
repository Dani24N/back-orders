<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\OrderController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/accounts',[AccountController::class,'getAllAccount']);
Route::get('/account/{idAccount}',[AccountController::class,'getAccount']);
Route::post('/account/create',[AccountController::class,'saveAccount']);
Route::post('/account/update',[AccountController::class,'updateAccount']);
Route::delete('/account/delete/{idAccount}',[AccountController::class,'deleteAccount']);
/**/
Route::get('/order/{idOrder}',[OrderController::class,'getOrder']);
Route::post('/order/create',[OrderController::class,'saveOrder']);
Route::post('/order/update',[OrderController::class,'updateOrder']);
Route::delete('/order/delete/{idOrder}',[OrderController::class,'deleteOrder']);



