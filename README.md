<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Ejecución de proyecto

## Creacion de Base de Datos
Crear la base de datos ya sean MySql, Postgresql o SqLServe
En el archivo .env se encuentra la configuración de la conección a la base de Datos
### Creacion de tablas
Ejecutar la siguiente line de comando para crear las tablas en BD ya creada
```
php artisan migrate
```
### Instalar paquetes de front
Ejecutar la siguiente line de comando con el fin de instalar las dependencia de front en laravel
```
npm install
```
### Ejecutar servicios
Se debe ejecutar el proyecto con el siguiente comando:
```
php artisan serve
```
a continuacion ejecutar websockets con el siguiente comando:
```
php artisan websocket:server
```
y por ultimo ejecutar el front con:
```
npm run dev
```
