<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Events\CreateOrder;

class OrderController extends Controller
{
    public function getOrder(Request $request){
        try{
            $id = $request->idOrder;
            $validator = Validator::make(["idOrder"=>$id],[
                'idOrder'=>'required|exists:order',
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $order = Order::where('idOrder',$id)->get();
            return response()->json($order, 200);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
    public function saveOrder(Request $request){
        try{
            $validator = Validator::make($request->all(),[
                "idAccount"=>"required|exists:account",
                "product"=>"required",
                "amount"=>"required|numeric|min:1",
                "value"=>"required|numeric|min:1",
                "total"=>"required|numeric|min:1",
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $register = new Order;
            $register->idAccount = $request->idAccount;
            $register->product = $request->product;
            $register->amount = $request->amount;
            $register->value = $request->value;
            $register->total = $request->total;
            $register->save();
            $account = Order::join('account', 'order.idAccount', '=', 'account.idAccount')->where('order.idOrder',$register->idOrder)->get();
            CreateOrder::dispatch(["data"=>$account[0]]);
            return response()->json(["message"=>"Created!","data"=>$account], 201);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
    public function updateOrder(Request $request){
        try{
           $validator = Validator::make($request->all(),[
                "idOrder"=>"required|exists:order",
                "product"=>"required",
                "amount"=>"required|numeric|min:1",
                "value"=>"required|numeric|min:1",
                "total"=>"required|numeric|min:1",
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $update = Order::find($request->idOrder);
            $update->idOrder = $request->idOrder;
            $update->product = $request->product;
            $update->amount = $request->amount;
            $update->value = $request->value;
            $update->total = $request->total;
            $update->save();
            return response()->json(["message"=>"Update!","data"=>$update], 200);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
    public function deleteOrder(Request $request){
        try{
            $id = $request->idOrder;
            $validator = Validator::make(["idOrder"=>$id],[
                'idOrder'=>'required|exists:order',
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $delete = Order::find($request->idOrder);
            $delete->delete();
            return response()->json(["message"=>"Delete!","data"=>$delete], 200);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
}
