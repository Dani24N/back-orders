<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{

    public function getAllAccount(){
        return response()->json(Account::all());
    }

    public function getAccount(Request $request){
        try{
            $id = $request->idAccount;
            $validator = Validator::make(["idAccount"=>$id],[
                'idAccount'=>'required|exists:account',
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $account = Account::where('idAccount',$id)->get();
            return response()->json($account, 200);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
    public function saveAccount(Request $request){
        try{
            $validator = Validator::make($request->all(),[
                "name"=>"required",
                "email"=>"required",
                "phone"=>"required|numeric|min:7|max:10",
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $register = new Account;
            $register->name = $request->name;
            $register->email = $request->email;
            $register->phone = $request->phone; 
            $register->save();
            return response()->json(["message"=>"Created!","data"=>$register], 201);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
    public function updateAccount(Request $request){
        try{
           $validator = Validator::make($request->all(),[
                'idAccount'=>'required|exists:account',
                "name"=>"required",
                "email"=>"required",
                "phone"=>"required|numeric|min:10|max:10",
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $update = Account::find($request->idAccount);
            $update->name = $request->name;
            $update->email = $request->email;
            $update->phone = $request->phone;
            $update->save();
            return response()->json(["message"=>"Update!","data"=>$update], 200);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
    public function deleteAccount(Request $request){
        try{
            $id = $request->idAccount;
            $validator = Validator::make(["idAccount"=>$id],[
                'idAccount'=>'required|exists:account',
            ]);
            if($validator->fails()){
                return response()->json(["message"=>$validator->errors()], 400);
            }
            $delete = Account::find($request->idAccount);
            $delete->delete();
            return response()->json(["message"=>"Delete!","data"=>$delete], 200);
        }catch(Exception $e){
            return response()->json(["message"=>$e->getMessage()], 500);
        }
    }
}
